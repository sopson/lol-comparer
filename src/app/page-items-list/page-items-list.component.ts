import { Component, OnInit } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Item } from '../riotApiClasses';

@Component({
  selector: 'app-page-items-list',
  templateUrl: './page-items-list.component.html',
  styleUrls: ['./page-items-list.component.css', '../app.component.css']
})
export class PageItemsListComponent implements OnInit {
    items: Item[];
    url: string;

    version: string;
    versionUrl: string;

    chosenMap = 11;
    filterItem = "";d

    pageIsLoaded = false;

    constructor(private http: HttpClient) { }

    ngOnInit() {
        this.versionUrl = "./assets/riotApiCommunicator.php?request=version";
        this.http.get<string>(this.versionUrl).subscribe(data => this.version = data);
        this.getItems();
    }

    getItems() {
        this.url = "./assets/riotApiCommunicator.php?request=itemList&itemMap="+this.chosenMap;

        this.http.get<Item[]>(this.url).subscribe(resp => {
            this.items = resp;
            this.pageIsLoaded = true;
        });
    }

    selectMapChange(value: number) {
        this.chosenMap = value;

        this.pageIsLoaded = false;

        this.getItems();
    }
}
