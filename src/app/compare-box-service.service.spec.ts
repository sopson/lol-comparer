import { TestBed, inject } from '@angular/core/testing';

import { CompareBoxServiceService } from './compare-box-service.service';

describe('CompareBoxServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CompareBoxServiceService]
    });
  });

  it('should be created', inject([CompareBoxServiceService], (service: CompareBoxServiceService) => {
    expect(service).toBeTruthy();
  }));
});
