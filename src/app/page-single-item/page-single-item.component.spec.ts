import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageSingleItemComponent } from './page-single-item.component';

describe('PageSingleItemComponent', () => {
  let component: PageSingleItemComponent;
  let fixture: ComponentFixture<PageSingleItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageSingleItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageSingleItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
