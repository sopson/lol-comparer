import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { PageMenuComponent } from './page-menu/page-menu.component';
import { PageFooterComponent } from './page-footer/page-footer.component';
import { PageSearchBlockComponent } from './page-search-block/page-search-block.component';
import { PageSinglePlayerComponent } from './page-single-player/page-single-player.component';
import { PageCompareTwoPlayersComponent } from './page-compare-two-players/page-compare-two-players.component';
import { PageChampionsListComponent } from './page-champions-list/page-champions-list.component';
import { PageItemsListComponent } from './page-items-list/page-items-list.component';
import { HomepageInfoComponent } from './homepage-info/homepage-info.component';
import { AppRoutingModule } from './/app-routing.module';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { PageSingleChampionComponent } from './page-single-champion/page-single-champion.component';
import { PageSingleItemComponent } from './page-single-item/page-single-item.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ChampionItemFilterPipe } from './champion-item-filter.pipe';
import { PageCompareBoxComponent } from './page-compare-box/page-compare-box.component';


@NgModule({
  declarations: [
    AppComponent,
    PageMenuComponent,
    PageFooterComponent,
    PageSearchBlockComponent,
    PageSinglePlayerComponent,
    PageCompareTwoPlayersComponent,
    PageChampionsListComponent,
    PageItemsListComponent,
    HomepageInfoComponent,
    PageNotFoundComponent,
    PageSingleChampionComponent,
    PageSingleItemComponent,
    ChampionItemFilterPipe,
    PageCompareBoxComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    NgbModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
