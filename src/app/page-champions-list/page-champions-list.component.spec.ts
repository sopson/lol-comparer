import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageChampionsListComponent } from './page-champions-list.component';

describe('PageChampionsListComponent', () => {
  let component: PageChampionsListComponent;
  let fixture: ComponentFixture<PageChampionsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageChampionsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageChampionsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
