import { Component, OnInit } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Champion } from '../riotApiClasses';

@Component({
  selector: 'app-page-champions-list',
  templateUrl: './page-champions-list.component.html',
  styleUrls: ['./page-champions-list.component.css', '../app.component.css']
})
export class PageChampionsListComponent implements OnInit {
    champions: Champion[];
    url: string;

    version: string;
    versionUrl: string;

    pageIsLoaded = false;

    filterChampion = "";

    constructor(private http: HttpClient) { }

    ngOnInit() {
        this.versionUrl = "./assets/riotApiCommunicator.php?request=version";
        this.http.get<string>(this.versionUrl).subscribe(data => this.version = data);
        this.getChampions();
    }

    getChampions() {
        this.url = "./assets/riotApiCommunicator.php?request=championList";

        this.http.get<Champion[]>(this.url).subscribe(resp => {
            this.champions = resp;
            this.pageIsLoaded = true;
        });
    }
}
