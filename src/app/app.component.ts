import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
    compareTwoPlayers = {
        player1: {
            summonerName: "",
            region: ""
        },
        player2: {
            summonerName: "",
            region: ""
        }
    }
}
