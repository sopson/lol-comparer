import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'championItemFilter'
})
export class ChampionItemFilterPipe implements PipeTransform
{
    transform(value: any, name: any): any
    {
        if(name === undefined)
        {
            return value;
        }

        return value.filter(function(value) {
            return value.name.toLowerCase().includes(name.toLowerCase());
        });
    }
}
