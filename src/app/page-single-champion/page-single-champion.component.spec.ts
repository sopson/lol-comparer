import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageSingleChampionComponent } from './page-single-champion.component';

describe('PageSingleChampionComponent', () => {
  let component: PageSingleChampionComponent;
  let fixture: ComponentFixture<PageSingleChampionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageSingleChampionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageSingleChampionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
