import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';

import { Champion } from '../riotApiClasses';

@Component({
  selector: 'app-page-single-champion',
  templateUrl: './page-single-champion.component.html',
  styleUrls: ['./page-single-champion.component.css', '../app.component.css']
})
export class PageSingleChampionComponent implements OnInit {
    champion: Champion;
    name: string;
    url: string;

    version: string;
    versionUrl: string;

    pageIsLoaded = false;

    constructor(private route: ActivatedRoute, private http: HttpClient) { }

    ngOnInit() {
        this.name = this.route.snapshot.paramMap.get('name');
        this.versionUrl = "./assets/riotApiCommunicator.php?request=version";
        this.http.get<string>(this.versionUrl).subscribe(data => this.version = data);
        this.getChampion();
    }

    getChampion() {
        this.url = "./assets/riotApiCommunicator.php?request=champion&championName="+this.name;

        this.http.get<Champion>(this.url).subscribe(resp => {
            this.champion = resp;
            this.pageIsLoaded = true;
        });
    }
}
