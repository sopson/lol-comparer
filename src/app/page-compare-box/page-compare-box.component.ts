import { Component, OnInit, Input } from '@angular/core';
import { CompareBoxServiceService } from '../compare-box-service.service';
import { comparePlayersClass } from '../riotApiClasses';

@Component({
  selector: 'app-page-compare-box',
  templateUrl: './page-compare-box.component.html',
  styleUrls: ['./page-compare-box.component.css'],
  providers: [ CompareBoxServiceService ]
})
export class PageCompareBoxComponent implements OnInit {

    compareTwoPlayers: comparePlayersClass;

    constructor(private recivedPlayers: CompareBoxServiceService) { }

    ngOnInit() {
        this.recivedPlayers.currentMessage.subscribe(message => this.compareTwoPlayers = message);
    }
}
