import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { comparePlayersClass } from './riotApiClasses';


@Injectable()
export class CompareBoxServiceService {

    message = {
        player1: {
            summonerName: "",
            summonerRegion: ""
        },
        player2: {
            summonerName:"",
            summonerRegion: ""
        }
    };

    private playersSource = new BehaviorSubject<comparePlayersClass>(this.message);
    public currentMessage = this.playersSource.asObservable();

    constructor() { }

    changeMessage(message: comparePlayersClass) {
        console.log(message);
        this.playersSource.next(message);
    }

}
