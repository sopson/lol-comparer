export interface Player {
    id: number;
    accountId: number;
    name: string;
    summonerLevel: number;
    profileIconId: number;
    revisionDate: number;
}

export interface Champion {
    id: number;
    key: string;
    name: string;
    title: string;
    allytips: string[];
    enemytips: string[];
    info: ChampionInfo;
    stats: ChampionStats;
    spells: ChampionSpells[];
}

export interface ChampionInfo {
    attack: number;
    defense: number;
    magic: number;
    difficulty: number;
}

export interface ChampionStats {
    armor: number;
    armorperlevel: number;
    attackdamage: number;
    attackdamageperlevel: number;
    attackrange: number;
    attackspeed: number;
    attackspeedperlevel: number;
    crit: number;
    critperlevel: number;
    hp: number;
    hpperlevel: number;
    hpregen: number;
    hpregenperlevel: number;
    movespeed: number;
    mp: number;
    mpperlevel: number;
    mpregen: number;
    mpregenperlevel: number;
    spellblock: number;
    spellblockperlevel: number;
}

export interface ChampionSpells {
    name: string;
    key: string;
    description: string;
    sanitizedDescription: string;
    tooltip: string;
    sanitizedTooltip: string;
    leveltip: object;
    image: object;
    resource: string;
    maxrank: number;
    cost: number[];
    costType: string;
    costBurn: string;
    cooldown: number[];
    colldownBurn: number;
    effect: object[];
    effectBurn: string[];
    vars: object[];
    range: number[];
    rangeBurn: number;
}

export interface Item {
    id: number;
    name: string;
    description: string;
    gold: ItemGold;
    plaintext: string;
    effect: object;
    stats: object;
}

export interface ItemGold {
    base: number;
    total: number;
    sell: number;
    purchasable: boolean;
}

export interface ItemMaps {
    11: boolean; // 11: Summoner's Rift
    12: boolean; // 12: Howling Abyss
    14: boolean; // 14: Butcher's Bridge
    16: boolean; // 16: Cosmic Ruins
    18: boolean; // 18: Valoran City Park
    8: boolean; // 8: The Crystal Scar
    19: boolean; // 19: Substructure 43
    10: boolean; // 10: Twisted Treeline
}

export interface SinglePlayerContainer {
    player: Player;
    matches: Match[];
    positions: MatchPositions;
    display_matches: MatchDetails[];
}

export interface Match {
    gameId: number;
    platformId: string;
    queue: number;
    season: number;
    timestamp: number;
    role: string;
    lane: string;
}

export interface MatchPositions {
    top: number;
    jungle: number;
    mid: number;
    bottom: number;
    support: number;
}

export interface MatchDetails {
    gameId: number;
    platformId: string;
    gameCreation: number;
    gameDuration: number;
    queueId: number;
    mapId: number;
    seasonId: number;
    gameVersion: string;
    gameMode: string;
    gameType: string;
    teams: MatchDetailsTeam[];
    participants: MatchDetailsParticipants[];
    participantIdentities: MatchDetailsParticipantsIndentities[];
}

export interface MatchDetailsTeam {
    teamId: number;
    win: string;
    firstBlood: boolean;
    firstTower: boolean;
    firstInhibitor: boolean;
    firstBaron: boolean;
    firstDragon: boolean;
    firstRiftHerald: boolean;
    towerKills: number;
    inhibitorKills: number;
    baronKills: number;
    dragonKills: number;
    vilemawKills: number;
    riftHeraldKills: number;
    dominionVictoryScore: number;
    bans: MatchDetailsTeamBans[];
}

export interface MatchDetailsTeamBans {
    championId: number;
    pickTurn: number;
}

export interface MatchDetailsParticipants {
    participantId: number;
    teamId: number;
    championId: number;
    spell1Id: number;
    spell2Id: number;
    highestAchievedSeasonTier: string;
    stats: MatchDetailsParticipantsStats;
    timeline: MatchDetailsParticipantsTimeline;
}

export interface MatchDetailsParticipantsStats {
    participantId: number;
    win: boolean;
    item0: number;
    item1: number;
    item2: number;
    item3: number;
    item4: number;
    item5: number;
    item6: number;
    kills: number;
    deaths: number;
    assists: number;
    largestKillingSpree: number;
    largestMultiKill: number;
    killingSprees: number;
    longestTimeSpentLiving: number;
    doubleKills: number;
    tripleKills: number;
    quadraKills: number;
    pentaKills: number;
    unrealKills: number;
    totalDamageDealt: number;
    magicDamageDealt: number;
    physicalDamageDealt: number;
    trueDamageDealt: number;
    largestCriticalStrike: number;
    totalDamageDealtToChampions: number;
    magicDamageDealtToChampions: number;
    physicalDamageDealtToChampions: number;
    trueDamageDealtToChampions: number;
    totalHeal: number;
    totalUnitsHealed: number;
    damageSelfMitigated: number;
    damageDealtToObjectives: number;
    damageDealtToTurrets: number;
    visionScore: number;
    timeCCingOthers: number;
    totalDamageTaken: number;
    magicalDamageTaken: number;
    physicalDamageTaken: number;
    trueDamageTaken: number;
    goldEarned: number;
    goldSpent: number;
    turretKills: number;
    inhibitorKills: number;
    totalMinionsKilled: number;
    neutralMinionsKilled: number;
    neutralMinionsKilledTeamJungle: number;
    neutralMinionsKilledEnemyJungle: number;
    totalTimeCrowdControlDealt: number;
    champLevel: number;
    visionWardsBoughtInGame: number;
    sightWardsBoughtInGame: number;
    wardsPlaced: number;
    wardsKilled: number;
    firstBloodKill: boolean;
    firstBloodAssist: boolean;
    firstTowerKill: boolean;
    firstTowerAssist: boolean;
    firstInhibitorKill: boolean;
    firstInhibitorAssist: boolean;
    combatPlayerScore: number;
    objectivePlayerScore: number;
    totalPlayerScore: number;
    totalScoreRank: number;
    playerScore0: number;
    playerScore1: number;
    playerScore2: number;
    playerScore3: number;
    playerScore4: number;
    playerScore5: number;
    playerScore6: number;
    playerScore7: number;
    playerScore8: number;
    playerScore9: number;
    perk0: number;
    perk0Var1: number;
    perk0Var2: number;
    perk0Var3: number;
    perk1: number;
    perk1Var1: number;
    perk1Var2: number;
    perk1Var3: number;
    perk2: number;
    perk2Var1: number;
    perk2Var2: number;
    perk2Var3: number;
    perk3: number;
    perk3Var1: number;
    perk3Var2: number;
    perk3Var3: number;
    perk4: number;
    perk4Var1: number;
    perk4Var2: number;
    perk4Var3: number;
    perk5: number;
    perk5Var1: number;
    perk5Var2: number;
    perk5Var3: number
    perkPrimaryStyle: number;
    perkSubStyle: number;
}

export interface MatchDetailsParticipantsTimeline {
    participantId: number;
    role: string;
    lane: string;
    creepsPerMinDeltas: any;
    xpPerMinDeltas: any;
    goldPerMinDeltas: any;
    csDiffPerMinDeltas: any;
    xpDiffPerMinDeltas: any;
    damageTakenPerMinDeltas: any;
    damageTakenDiffPerMinDeltas: any;
}

export interface MatchDetailsParticipantsIndentities {
    participantId: number;
    player: MatchDetailsParticipantsIndentitiesPlayer;
}

export interface MatchDetailsParticipantsIndentitiesPlayer {
    platformId: string;
    accountId: number;
    summonerName: string;
    summonerId: number;
    currentPlatformId: string;
    currentAccountId: number;
    matchHistoryUri: string;
    profileIcon: number;
}

export interface comparePlayersClass {
    player1: {
        summonerName: string,
        summonerRegion: string
    },
    player2: {
        summonerName: string,
        summonerRegion: string
    }
}
