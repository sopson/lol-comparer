import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PageItemsListComponent } from './page-items-list/page-items-list.component'
import { PageSingleItemComponent } from './page-single-item/page-single-item.component'
import { PageSingleChampionComponent } from './page-single-champion/page-single-champion.component'
import { PageChampionsListComponent } from './page-champions-list/page-champions-list.component'
import { PageSinglePlayerComponent } from './page-single-player/page-single-player.component'
import { HomepageInfoComponent } from './homepage-info/homepage-info.component'
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

const routes: Routes = [
    { path: '', redirectTo: '/home', pathMatch: 'full' },
    { path: 'home', component: HomepageInfoComponent },
    { path: 'items', component: PageItemsListComponent },
    { path: 'items/:name', component: PageSingleItemComponent },
    { path: 'champions', component: PageChampionsListComponent },
    { path: 'champions/:name', component: PageSingleChampionComponent },
    { path: 'search/region/:region/name/:name', component: PageSinglePlayerComponent },
    { path: '**', component: PageNotFoundComponent }
];


@NgModule({
    imports: [ RouterModule.forRoot(routes /*, { enableTracing: true }*/) ],
    exports: [ RouterModule ]
})
export class AppRoutingModule {}
