import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageSinglePlayerComponent } from './page-single-player.component';

describe('PageSinglePlayerComponent', () => {
  let component: PageSinglePlayerComponent;
  let fixture: ComponentFixture<PageSinglePlayerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageSinglePlayerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageSinglePlayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
