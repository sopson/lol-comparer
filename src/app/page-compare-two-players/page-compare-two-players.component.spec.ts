import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageCompareTwoPlayersComponent } from './page-compare-two-players.component';

describe('PageCompareTwoPlayersComponent', () => {
  let component: PageCompareTwoPlayersComponent;
  let fixture: ComponentFixture<PageCompareTwoPlayersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageCompareTwoPlayersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageCompareTwoPlayersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
