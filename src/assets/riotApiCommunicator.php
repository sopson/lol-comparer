<?php

$obj = [];

$api_key = "api_key=RGAPI-a1597582-3b9b-45ee-b7f9-fc3d4bb0798b";

if ($_GET["request"] == "championList") 
{
    $champions = [];

    $champions_path = "./api/champions";
    $dir = opendir($champions_path);

    while (false !== ($parent = readdir($dir))) 
    {
        if (($parent != '.') && ($parent != '..')) 
        {
            $champion_dir = opendir($champions_path.'/'.$parent);

            while (false !== ($file = readdir($champion_dir))) 
            {
                if (($file != '.') && ($file != '..')) 
                {
                    $champions[] = json_decode(file_get_contents($champions_path.'/'.$parent.'/'.$file));
                }
            }
        }
    }
    
    $obj = $champions;
}
elseif ($_GET['request'] == "champion" && !empty($_GET['championName'])) 
{
    $champion_path = "./api/champions/".$_GET['championName'].'/'.$_GET['championName'].'.json';

    $obj = json_decode(file_get_contents($champion_path));
}
elseif ($_GET["request"] == "itemList" && isset($_GET['itemMap'])) 
{
    $items = [];

    $items_path = "./api/items";
    $dir = opendir($items_path);

    while (false !== ($parent = readdir($dir))) 
    {
        if (($parent != '.') && ($parent != '..')) 
        {
            $item_dir = opendir($items_path.'/'.$parent);

            while (false !== ($file = readdir($item_dir))) 
            {
                if (($file != '.') && ($file != '..')) 
                {
                    $tmpItem = json_decode(file_get_contents($items_path.'/'.$parent.'/'.$file));

                    if ($tmpItem->maps->{$_GET['itemMap']} === true) 
                    {
                        $items[] = $tmpItem;
                    }
                }
            }
        }
    }

    $obj = $items;
} 
elseif ($_GET["request"] == "getItem" && !empty($_GET["itemName"])) 
{
    $item_path = "./api/items/".$_GET["itemName"]."/".$_GET["itemName"].".json";

    $obj = json_decode(file_get_contents($item_path));
} 
elseif ($_GET["request"] == "getPlayer" && !empty($_GET['region']) && !empty($_GET['summoner_name'])) 
{
    $region = [
        "BR"    => "br1",
        "EUNE"  => "eun1",
        "EUW"   => "euw1",
        "JP"    => "jp1",
        "KR"    => "kr",
        "LAN"   => "la1",
        "LAS"   => "la2",
        "NA"    => "na1",
        "OCE"   => "oc1",
        "TR"    => "tr1",
        "RU"    => "ru"
    ];

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $rUrl = "https://".$region[$_GET['region']].".api.riotgames.com/lol/summoner/v3/summoners/by-name/".$_GET['summoner_name']."?";
    curl_setopt($ch, CURLOPT_URL, $rUrl.$api_key);

    $curl_data = json_decode(curl_exec($ch));
    curl_close($ch);
    
    if ($curl_data->error == null && $curl_data->status->status_code == null) 
    {
        $obj['player'] = $curl_data;

        $player = $curl_data;

        $dir = './api/players/'.$_GET['region'];

        if (file_exists($dir."/".$player->name.'/'.$player->name.".json") && !isset($_GET['getPlayerType'])) {
            $obj = json_decode(file_get_contents($dir."/".$player->name."/".$player->name.".json"));

            $tmp = [];
            //qpp($obj);
            $i = 0;
            foreach ($obj->matches as $match) 
            {
                if (file_exists("./api/matches/".$_GET['region'].'/'.$match->gameId.".json")) 
                {
                    $tmp[] = json_decode(file_get_contents("./api/matches/".$_GET['region'].'/'.$match->gameId.".json"));
                    
                    if($i == 9)
                    {
                        break;
                    }
                    
                    $i++;
                }
            }

            $obj->display_matches = $tmp;
        } 
        else 
        {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $rUrl = "https://".$region[$_GET['region']].".api.riotgames.com/lol/match/v3/matchlists/by-account/".$player->accountId.'?';

            curl_setopt($ch, CURLOPT_URL, $rUrl.$api_key);

            $matches = json_decode(curl_exec($ch));

            curl_close($ch);

            $obj['matches'] = $matches->matches;
            
            if (!file_exists($dir."/".$player->name)) 
            {
                mkdir($dir."/".$player->name, 0777, true);
            } 
            else 
            {
                rrmdir($dir."/".$player->name);
                mkdir($dir."/".$player->name, 0777, true);
            }

            $top_count = 0;
            $jungl_count = 0;
            $mid_count = 0;
            $bot_count = 0;
            $supp_count = 0;

            foreach ($matches->matches as $match) 
            {
                switch ($match->lane) 
                {
                    case "TOP":
                        $top_count++;
                    break;
                    case "JUNGLE":
                        $jungl_count++;
                    break;
                    case "MID":
                        $mid_count++;
                    break;
                    case "BOTTOM":
                        if($match->role == "DUO_SUPPORT") {
                            $supp_count++;
                        }else {
                            $bot_count++;   
                        }
                    break;
                    case "SUPPORT":
                        $supp_count++;
                    break;
                }
            }

            $obj['positions'] = array(
                "top" => $top_count,
                "jungle" => $jungl_count,
                "mid" => $mid_count,
                "bottom" => $bot_count,
                "support" => $supp_count
            );

            $tmp = json_encode($obj);

            $file = fopen($dir."/".$player->name."/".$player->name.".json", "w");
            fwrite($file, $tmp);
            fclose($file);

            $obj['display_matches'] = [];

            for ($i = 0; $i < 10; $i++) 
            {
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                $rUrl = "https://".$region[$_GET['region']].".api.riotgames.com/lol/match/v3/matches/".$matches->matches[$i]->gameId.'?';

                curl_setopt($ch, CURLOPT_URL, $rUrl.$api_key);

                $single_match = json_decode(curl_exec($ch));

                $obj['display_matches'][] = $single_match;

                curl_close($ch);

                $save_dir_match = "./api/matches/".$_GET['region'].'/'.$single_match->gameId.".json";

                if (!file_exists($save_dir_match)) 
                {
                    $file = fopen($save_dir_match, "w");
                    fwrite($file, json_encode($single_match));
                    fclose($file);
                }
            }
        }
    } 
    else 
    {
        $obj = $curl_data;
    }
} 
elseif ($_GET['request'] == "version") 
{
    $version_path = "./api/version.json";
    $obj = json_decode(file_get_contents($version_path));
}

$obj = json_encode($obj);

print_r($obj);
//qpp($obj);
die();

function rrmdir($src) {
    $dir = opendir($src);
    while (false !== ($file = readdir($dir))) {
        if (($file != '.') && ($file != '..')) {
            $full = $src . '/' . $file;
            if (is_dir($full)) {
                rrmdir($full);
            } else {
                unlink($full);
            }
        }
    }
    closedir($dir);
    rmdir($src);
}


function qpp($str) {
    print_r("<pre>");
    print_r($str);
    print_r("</pre>");
}
