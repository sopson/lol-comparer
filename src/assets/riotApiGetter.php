<?php

$api_key = "api_key=RGAPI-a1597582-3b9b-45ee-b7f9-fc3d4bb0798b";

$ch = curl_init();
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

// Getting champion list
$rUrl = "https://euw1.api.riotgames.com/lol/static-data/v3/champions?locale=en_US&champListData=allytips&champListData=enemytips&champListData=info&champListData=spells&champListData=stats&dataById=false&";

curl_setopt($ch, CURLOPT_URL, $rUrl.$api_key);

$curl_data = curl_exec($ch);
$curl_data = json_decode($curl_data);

if (!file_exists("./api")) {
    mkdir("./api", 0777, true);
}

$region = [
    "BR",
    "EUNE",
    "EUW",
    "JP",
    "KR",
    "LAN",
    "LAS",
    "NA",
    "OCE",
    "TR",
    "RU"
];

if (!file_exists("./api/players")) {
    mkdir("./api/players", 0777, true);
}

if (!file_exists("./api/matches")) {
    mkdir("./api/matches", 0777, true);
}

for ($i=0; $i < count($region); $i++) {
    if (!file_exists("./api/players/".$region[$i])) {
        mkdir("./api/players/".$region[$i], 0777, true);
    }

    if (!file_exists("./api/matches/".$region[$i])) {
        mkdir("./api/matches/".$region[$i], 0777, true);
    }
}


if (!file_exists("./api/chmapions")) {
    mkdir("./api/champions", 0777, true);
}

if ($curl_data->data != null) {
    foreach ($curl_data->data as $champion) {
        $dir = "./api/champions/".$champion->key;

        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        } else {
            rrmdir($dir);
            mkdir($dir, 0777, true);
        }

        $save_data = json_encode($champion);
        $file = fopen($dir."/".$champion->key.".json", "w");
        fwrite($file, $save_data);
        fclose($file);
    }
    curl_close($ch);
} else {
    print_r($curl_data);
    curl_close($ch);
    die();
}
// -------------- END OF GETTING CHAMPIONS --------------

// Getting items list
if (!file_exists("./api/items")) {
    mkdir("./api/items", 0777, true);
}

$rUrl = "https://euw1.api.riotgames.com/lol/static-data/v3/items?locale=en_US&itemListData=effect&itemListData=gold&itemListData=maps&itemListData=stats&";

$ch = curl_init();
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

curl_setopt($ch, CURLOPT_URL, $rUrl.$api_key);

$curl_data = curl_exec($ch);
$curl_data = json_decode($curl_data);

if ($curl_data->data != null) {
    foreach ($curl_data->data as $item) {
        $file_name = str_replace(" ", "_", $item->name);

        $dir = "./api/items/".$file_name;

        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        } else {
            rrmdir($dir);
            mkdir($dir, 0777, true);
        }

        $save_data = json_encode($item);
        $file = fopen($dir."/".$file_name.".json", "w");
        fwrite($file, $save_data);
        fclose($file);
    }
    curl_close($ch);
} else {
    print_r($curl_data);
    curl_close($ch);
    die();
}
// -------------- END OF GETTING ITEMS --------------


// Getting latest version
$rUrl = "https://eun1.api.riotgames.com/lol/static-data/v3/versions?";


$ch = curl_init();
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

curl_setopt($ch, CURLOPT_URL, $rUrl.$api_key);

$curl_data = curl_exec($ch);
$curl_data = json_decode($curl_data);

$apiVersion = $curl_data[0];
$apiVersion= json_encode($apiVersion);

$file = fopen("./api/version.json", "w");
fwrite($file, $apiVersion);
fclose($file);
curl_close($ch);

// -------------- END OF GETTING LATEST VERSION --------------

function rrmdir($src)
{
    $dir = opendir($src);
    while (false !== ($file = readdir($dir))) {
        if (($file != '.') && ($file != '..')) {
            $full = $src . '/' . $file;
            if (is_dir($full)) {
                rrmdir($full);
            } else {
                unlink($full);
            }
        }
    }
    closedir($dir);
    rmdir($src);
}
